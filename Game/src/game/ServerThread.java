package game;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ServerThread extends Thread {
    Socket connSocket;
    Common common;
    String[] board = Main.getBoard();
    Player me;

    public ServerThread(Socket connSocket, Common common) {
        this.connSocket = connSocket;
        this.common = Common.getInstance();
    }

    @Override
    public void run() {
        try {
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connSocket.getOutputStream());
//            String playerName = inFromClient.readLine();

            while (true) {

                String clientSentence = inFromClient.readLine();
                //Unpacking
                String[] command = clientSentence.split(" ");

                if (command[0].equals("S")) {
                    String name = command[1];
                    int x = Integer.parseInt(command[2]);
                    int y = Integer.parseInt(command[3]);

                    me = new Player(name, x, y, "up");
//                    common.addPlayer(me);

                    clientSentence = "S " + me.getXpos() + " " + me.getYpos() + " " + name + "!" + "\n";
                }

                //Beregner ny stilling
                if (command[0].equals("M")) {
                    int x = me.getXpos();
                    int y = me.getYpos();
                    String direction = command[3];
                    int delta_x = Integer.parseInt(command[1]);
                    int delta_y = Integer.parseInt(command[2]);
                    String name = command[4];

                    if (board[y + delta_y].charAt(x + delta_x) == 'w') {
                        me.addPoints(-1);
                    }

                    else {
                        Player p = getPlayerAt(x + delta_x, y + delta_y);
                        if (p != null) {
                            me.addPoints(10);
                            p.addPoints(-10);
                        } else {
                            me.addPoints(1);
                            me.setXpos(x + delta_x);
                            me.setYpos(y + delta_y);
                            me.setDirection(direction);
                        }
                    }

                    StringBuilder sb = new StringBuilder();
//                    for (Player p : common.players) {
                    sb.append(
                            "M " + me.getXpos() + " " + me.getYpos() + " " + me.getDirection()
                                    + " " + me.point + " " + name + "!");
//                    }
                    sb.append("\n");
                    System.out.println(sb);
                    clientSentence = sb.toString();

                }

                //Send streng til alle klienter
                outToClient.writeBytes(clientSentence);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // do the work here
    }

//-----------------------------------------------------------------
    private Player getPlayerAt(int x, int y) {
        for (Player p : common.getPlayers()) {
            if (p.getXpos() == x && p.getYpos() == y) {
                return p;
            }
        }
        return null;
    }

}
