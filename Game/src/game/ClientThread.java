package game;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

public class ClientThread extends Thread {
    Socket connSocket;
    Common common;
    Label[][] fields = Main.getFields();
    private ArrayList<Player> players;
    private Player me;

    public ClientThread(Socket connSocket, Common common) {
        this.connSocket = connSocket;
        this.common = Common.getInstance();
        fields = Main.getFields();
        this.players = new ArrayList<>();
    }

    @Override
    public void run() {
        try {
            while (true) {

                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connSocket.getInputStream()));
                DataOutputStream outToClient = new DataOutputStream(connSocket.getOutputStream());
                String clientSentence = inFromClient.readLine();

                String[] commands = clientSentence.split("!");

                for (String s : commands) {
                    String[] data = s.split(" ");
                    int x = Integer.parseInt(data[1]);
                    int y = Integer.parseInt(data[2]);

                    if (s.charAt(0) == 'S') {
                        String playerName = data[3];
                        me = new Player(playerName, x, y, "up");
                        common.addPlayer(me);
                    }

                    for (Player p : common.getPlayers()) {
                        Platform.runLater(() -> {
                            fields[p.getXpos()][p.getYpos()].setGraphic(new ImageView(Main.image_floor));
                        });
                    }

                    if (s.charAt(0) == 'M') {
                        String name = data[5];
                        Player p = common.getPlayerByName(name);

                        String direction = data[3];
                        int point = Integer.parseInt(data[4]);

                        p.setXpos(x);
                        p.setYpos(y);
                        p.setDirection(direction);
                        p.point = point;
                    }
                }

                for (Player p : common.players) {
                    Platform.runLater(() -> {
                        if (p.getDirection().equals("right")) {
                            fields[p.getXpos()][p.getYpos()].setGraphic(new ImageView(Main.hero_right));

                        }

                        if (p.getDirection().equals("left")) {
                            fields[p.getXpos()][p.getYpos()].setGraphic(new ImageView(Main.hero_left));
                        }

                        if (p.getDirection().equals("up")) {
                            fields[p.getXpos()][p.getYpos()].setGraphic(new ImageView(Main.hero_up));
                        }

                        if (p.getDirection().equals("down")) {
                            fields[p.getXpos()][p.getYpos()].setGraphic(new ImageView(Main.hero_down));
                        }

                    });

                    Platform.runLater(() -> {
                        Main.scoreList.setText(getScoreList());
                    });
                }
            }

        } catch (

        IOException e) {
            e.printStackTrace();
        }
        // do the work here

    }

    public String getScoreList() {
        StringBuffer b = new StringBuffer(100);
        for (Player p : common.players) {
            b.append(p + "\r\n");
        }
        return b.toString();
    }

//    private Player getPlayerAt(int x, int y) {
//        for (Player p : players) {
//            if (p.getXpos() == x && p.getYpos() == y) {
//                return p;
//            }
//        }
//        return null;
//    }
}
