package game;

import java.util.ArrayList;
import java.util.List;

public class Common {

    private static Common uniqueInstance;
    static ArrayList<Player> players;

    private Common() {
    }

    // Singleton pattern
    public static Common getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Common();
            players = new ArrayList<>();
        }

        return uniqueInstance;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void addPlayer(Player player) {
        Common.players.add(player);
    }

    public Player getPlayerAt(int x, int y) {
        for (Player p : players) {
            if (p.getXpos() == x && p.getYpos() == y) {
                return p;
            }
        }
        return null;
    }

    public Player getPlayerByName(String string) {
        for (Player p : players) {
            if (p.name.equals(string)) {
                return p;
            }
        }
        return null;
    }

}
